﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MovilCOP_C3.Web.Models
{
    public class Paises
    {
        public int Id { get; set; }

        #region Nombre País 
        [Display(Name = "País")]
        [Required]
        public string V_Nombre { get; set; }
        #endregion

        #region Imagen Path de Pais
        [Display(Name = "Imagen")]
        [Required]
        public string V_ImagenPath { get; set; }
        #endregion

        #region Departamento - pais
        public virtual ObservableCollection<Departamentos> Departamento { get; set; }
        #endregion
    }
}