﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MovilCOP_C3.Web.Models;

namespace MovilCOP_C3.Web.Controllers
{
    public class MenusHijosController : Controller
    {
        private MovilCOP_C3WebContext db = new MovilCOP_C3WebContext();

        // GET: MenusHijos
        public ActionResult Index()
        {
            var menusHijos = db.MenusHijos.Include(m => m.MenuPadre);
            return View(menusHijos.ToList());
        }

        // GET: MenusHijos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MenusHijos menusHijos = db.MenusHijos.Find(id);
            if (menusHijos == null)
            {
                return HttpNotFound();
            }
            return View(menusHijos);
        }

        // GET: MenusHijos/Create
        public ActionResult Create()
        {
            ViewBag.MenuPadreId = new SelectList(db.MenusPadres, "Id", "V_Nombre");
            return View();
        }

        // POST: MenusHijos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,MenuPadreId,V_Nombre")] MenusHijos menusHijos)
        {
            if (ModelState.IsValid)
            {
                db.MenusHijos.Add(menusHijos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MenuPadreId = new SelectList(db.MenusPadres, "Id", "V_Nombre", menusHijos.MenuPadreId);
            return View(menusHijos);
        }

        // GET: MenusHijos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MenusHijos menusHijos = db.MenusHijos.Find(id);
            if (menusHijos == null)
            {
                return HttpNotFound();
            }
            ViewBag.MenuPadreId = new SelectList(db.MenusPadres, "Id", "V_Nombre", menusHijos.MenuPadreId);
            return View(menusHijos);
        }

        // POST: MenusHijos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,MenuPadreId,V_Nombre")] MenusHijos menusHijos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(menusHijos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MenuPadreId = new SelectList(db.MenusPadres, "Id", "V_Nombre", menusHijos.MenuPadreId);
            return View(menusHijos);
        }

        // GET: MenusHijos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MenusHijos menusHijos = db.MenusHijos.Find(id);
            if (menusHijos == null)
            {
                return HttpNotFound();
            }
            return View(menusHijos);
        }

        // POST: MenusHijos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MenusHijos menusHijos = db.MenusHijos.Find(id);
            db.MenusHijos.Remove(menusHijos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
