﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MovilCOP_C3.Web.Models;

namespace MovilCOP_C3.Web.Controllers
{
    public class MenusPadresController : Controller
    {
        private MovilCOP_C3WebContext db = new MovilCOP_C3WebContext();

        // GET: MenusPadres
        public ActionResult Index()
        {
            return View(db.MenusPadres.ToList());
        }

        // GET: MenusPadres/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MenusPadres menusPadres = db.MenusPadres.Find(id);
            if (menusPadres == null)
            {
                return HttpNotFound();
            }
            return View(menusPadres);
        }

        // GET: MenusPadres/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MenusPadres/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,V_Nombre")] MenusPadres menusPadres)
        {
            if (ModelState.IsValid)
            {
                db.MenusPadres.Add(menusPadres);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(menusPadres);
        }

        // GET: MenusPadres/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MenusPadres menusPadres = db.MenusPadres.Find(id);
            if (menusPadres == null)
            {
                return HttpNotFound();
            }
            return View(menusPadres);
        }

        // POST: MenusPadres/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,V_Nombre")] MenusPadres menusPadres)
        {
            if (ModelState.IsValid)
            {
                db.Entry(menusPadres).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(menusPadres);
        }

        // GET: MenusPadres/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MenusPadres menusPadres = db.MenusPadres.Find(id);
            if (menusPadres == null)
            {
                return HttpNotFound();
            }
            return View(menusPadres);
        }

        // POST: MenusPadres/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MenusPadres menusPadres = db.MenusPadres.Find(id);
            db.MenusPadres.Remove(menusPadres);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
