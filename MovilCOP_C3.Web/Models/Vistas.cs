﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MovilCOP_C3.Web.Models
{
    public class Vistas
    {
        public int Id { get; set; }

        #region Id de el menú hijo
        [Display(Name = "Item Hijo")]
        [Required]
        public int MenuHijoId { get; set; }
        #endregion

        #region Nombre de la vista (Titulo H4)
        [Display(Name = "Nombre de vista")]
        [Required]
        public string V_Nombre { get; set; }
        #endregion

        #region Accion (Index, Create, Details..)
        [Display(Name = "Acción")]
        [Required]
        public string V_Accion { get; set; }
        #endregion

        #region controlador de la vista
        [Display(Name = "Controlador")]
        [Required]
        public string V_Controlador { get; set; }
        #endregion

        #region conexión virtual Menu hijo - vista
        public virtual MenusHijos MenuHijo { get; set; }
        #endregion
    }
}