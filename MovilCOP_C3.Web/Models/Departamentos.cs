﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MovilCOP_C3.Web.Models
{
    public class Departamentos
    {
        public int ID { get; set; }

        #region Pais Id
        [Display(Name = "Pais")]
        [Required]
        public int PaisId { get; set; }// A que persona corresponde al usuario
        #endregion

        #region NombreDepartamento
        [Display(Name = "Nombre del Departamento")]
        [Required]
        public string V_Nombre { get; set; }
        #endregion

        #region Image Path de Departamento
        [Display(Name = "Imagen Path")]
        [Required]
        public string V_ImagenPath { get; set; }
        #endregion

        #region conexión virtual Dep. -Pais
        public virtual Paises Pais { get; set; }
        #endregion

        #region Ciudad - departamento
        public virtual ObservableCollection<Ciudades> Ciudad { get; set; }
        #endregion
    }
}