﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MovilCOP_C3.Web.Models
{
    public class Usuarios
    {
        public int Id { get; set; }

        #region Persona Id
        [Display(Name = "Persona")]
        [Required]
        public int PersonaId { get; set; }// A que persona corresponde al usuario
        #endregion

        #region Nombre usuario
        [Display(Name = "Usuario")]
        [MinLength(6, ErrorMessage = "El Nombre de usuario debe ser superior a 6 caracteres")]
        [Required]        
        public string V_NombreUsu { get; set; }
        #endregion

        #region Contraseña
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]        
        [Required]
        public string V_Contrasenna { get; set; }
        #endregion

        #region confirmar contraseña
        [Compare("V_Contrasenna")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirme Contraseña")]                
        [Required]
        public string V_ContrasennaConf { get; set; }
        #endregion

        #region conexión virtual Usuario - Persona
        public virtual Personas Persona { get; set; }
        #endregion
    }
}