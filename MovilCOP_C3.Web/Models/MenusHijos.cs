﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MovilCOP_C3.Web.Models
{
    public class MenusHijos
    {
        public int Id { get; set; }

        #region Menu padre Id
        [Display(Name = "Menú Padre")]
        [Required]
        public int MenuPadreId { get; set; }// A que persona corresponde al usuario
        #endregion

        #region nombre menu hijo (item del dropdown)
        [Display(Name = "Nombre Item")]
        [Required]
        public string V_Nombre { get; set; }
        #endregion

        #region conexión virtual con Menu hijo - menu padre
        public virtual MenusPadres MenuPadre { get; set; }
        #endregion

        #region collection de Vistas
        public virtual ObservableCollection<Vistas> Vista { get; set; }
        #endregion

    }
}