﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MovilCOP_C3.Web.Models
{
    public class MovilCOP_C3WebContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public MovilCOP_C3WebContext() : base("name=MovilCOP_C3WebContext")
        {
            Database.SetInitializer<MovilCOP_C3WebContext>(
                   new DropCreateDatabaseIfModelChanges<MovilCOP_C3WebContext>()
             );
            //Recrea la base de datos con respecto al modelo de datos que tenemos en el proyecto
            //en produccion se debera crear una strategis como MigrateDatabaseToLatestVersion o similares
        }

        public DbSet<Personas> Personas { get; set; }

        public DbSet<Usuarios> Usuarios { get; set; }

        public DbSet<Paises> Paises { get; set; }

        public DbSet<Departamentos> Departamentos { get; set; }

        public DbSet<Ciudades> Ciudades { get; set; }

        public System.Data.Entity.DbSet<MovilCOP_C3.Web.Models.MenusPadres> MenusPadres { get; set; }

        public System.Data.Entity.DbSet<MovilCOP_C3.Web.Models.MenusHijos> MenusHijos { get; set; }

        public System.Data.Entity.DbSet<MovilCOP_C3.Web.Models.Vistas> Vistas { get; set; }
    }
}
