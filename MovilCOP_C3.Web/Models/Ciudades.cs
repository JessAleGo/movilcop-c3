﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MovilCOP_C3.Web.Models
{
    public class Ciudades
    {
        public int Id { get; set; }

        #region Departamento Id
        [Display(Name = "Departamento")]
        [Required]
        public int DepartamentoId { get; set; }// A que persona corresponde al usuario
        #endregion

        #region Nombre ciudad
        [Display(Name = "Nombre Ciudad")]
        [Required]
        public string V_Nombre { get; set; }
        #endregion

        #region imagen corespondiente
        [Display(Name = "Path de la Imagen")]
        [Required]
        public string V_ImagenPath { get; set; }
        #endregion

        #region conexión virtual Ciudad - Dep
        public virtual Departamentos Departamento { get; set; }
        #endregion
    }
}