﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MovilCOP_C3.Web.Models;

namespace MovilCOP_C3.Web.Controllers
{
    public class VistasController : Controller
    {
        private MovilCOP_C3WebContext db = new MovilCOP_C3WebContext();

        // GET: Vistas
        public ActionResult Index()
        {
            var vistas = db.Vistas.Include(v => v.MenuHijo);
            return View(vistas.ToList());
        }

        // GET: Vistas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vistas vistas = db.Vistas.Find(id);
            if (vistas == null)
            {
                return HttpNotFound();
            }
            return View(vistas);
        }

        // GET: Vistas/Create
        public ActionResult Create()
        {
            ViewBag.MenuHijoId = new SelectList(db.MenusHijos, "Id", "V_Nombre");
            return View();
        }

        // POST: Vistas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,MenuHijoId,V_Nombre,V_Accion,V_Controlador")] Vistas vistas)
        {
            if (ModelState.IsValid)
            {
                db.Vistas.Add(vistas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MenuHijoId = new SelectList(db.MenusHijos, "Id", "V_Nombre", vistas.MenuHijoId);
            return View(vistas);
        }

        // GET: Vistas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vistas vistas = db.Vistas.Find(id);
            if (vistas == null)
            {
                return HttpNotFound();
            }
            ViewBag.MenuHijoId = new SelectList(db.MenusHijos, "Id", "V_Nombre", vistas.MenuHijoId);
            return View(vistas);
        }

        // POST: Vistas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,MenuHijoId,V_Nombre,V_Accion,V_Controlador")] Vistas vistas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vistas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MenuHijoId = new SelectList(db.MenusHijos, "Id", "V_Nombre", vistas.MenuHijoId);
            return View(vistas);
        }

        // GET: Vistas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vistas vistas = db.Vistas.Find(id);
            if (vistas == null)
            {
                return HttpNotFound();
            }
            return View(vistas);
        }

        // POST: Vistas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Vistas vistas = db.Vistas.Find(id);
            db.Vistas.Remove(vistas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
