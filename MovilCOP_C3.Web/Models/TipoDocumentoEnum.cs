﻿namespace MovilCOP_C3.Web.Models
{
    public enum TipoDocumentoEnum
    {
        CedulaDeCiudadania,
        CedulaDeExtranjeria,        
        Pasaporte,
        TarjetaDeIdentidad,
    }
}