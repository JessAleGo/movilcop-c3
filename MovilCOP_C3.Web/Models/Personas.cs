﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MovilCOP_C3.Web.Models
{
    public class Personas
    {
        public int Id { get; set; }

        #region Numero de Documento
        [Display(Name ="Numero de Documento")]// Maquillaje para los campos y listas
        [Required]// Realizar validaciones en el formulario
        public int N_NumeroDocumento { get; set; }
        #endregion

        #region Nombre y apellido
        [Display(Name = "Nombre y Apellido")]
        [Required]
        public string V_NombresApellidos { get; set; }
        #endregion

        #region Correo
        [Display(Name = "Correo electronico")]
        [Required]
        [EmailAddress]
        public string V_Correo { get; set; }
        #endregion

        #region telefono
        [Display(Name = "Teléfono")]
        public string V_Telefono { get; set; }
        #endregion

        #region tipo documento Enum
        [Display(Name = "Tipo de Documento")]
        public TipoDocumentoEnum N_TipoDocumento { get; set; }
        //Enumeración para efectos practicos. Aplica para datos constantes que se puedan seleccionar,
        //como genero o tipo de documentos como en este caso
        #endregion

        #region Collection de Personas
        public virtual ObservableCollection<Usuarios> Usuario { get; set; }
        #endregion

    }
}