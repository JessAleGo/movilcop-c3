﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MovilCOP_C3.Web.Models
{
    public class MenusPadres
    {
        public int Id { get; set; }

        #region nombre de menu principal
        [Display(Name = "Nombre Menú")]
        [Required]
        public string V_Nombre { get; set;}
        #endregion

        #region collecion de menu hijo ('dropdown item')
        [Display(Name = "Item Hijo")]
        public virtual ObservableCollection<MenusHijos> MenuHijo { get; set; }
        #endregion
    }
}